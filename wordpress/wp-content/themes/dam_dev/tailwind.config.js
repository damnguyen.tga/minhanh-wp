/** @type {import('tailwindcss').Config} */
module.exports = {
  purge: [
      './inc/**/*.php',
      './js/**/*.js',
      './sass/**/*.scss',
      // './template-parts/**/*.php',
  ],
  content: ["./template-parts/*.{php,html,js}","./*.{php,html,js}"],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}